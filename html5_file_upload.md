# HTML5 File Upload


## Upload

This breaks down into:

* Getting the FileList from fileinput.files
* Sending the file contents using FormData

As a flight component an event can be triggered on the file input `change` event.

Another alternative is to use `FileReader.readAsBinary` and `XmlHttpRequest.sendAsBinary`,
but [I read somewhere][] that is only a good idea for small files. That would allow decoupling
the handelr from the `event.target` though.

All this component does is send the request and triggere upload progress related events
which UI elements can listen for.

An "upload" can contain multiple files, but in that case the progress events show progress
for the combined total of the files as all in one request. Some option could be added to
tell it to send each file in a separate request, then each file's progress could be monitored
at the cost of more HTTP requests.

Another useful option would be whether to instantly send the file, or wait for some user confirmation.

Note that any 404 or other server error will not be triggered until progress is complete, as
all your monitoring is the sending of the request.

Validation could be done by the thing that triggers the event, or by the data component. For
example, checking that it is an acceptable file type.

As the data component is written as a mixin this could be done with advice by doing something
`before` the upload is queued (or maybe `around`).

## Upload Progress

This is a UI component that listens for the progress events, which let you know the number
of bytes transferred and calculate a percentage.

## Preview

This breaks down into:

* Using the FileReader API to get the contents of the file
* Converting it to a `data:` URI to display on the page
* Any other manipulation, like resizing for a preview image

As with the upload this can be triggered on the file inputs `change` event. It
would also need to get at the `FileList` with `event.target.files`.

This could trigger events for reading progress as well, as that ma take a while
on large files.

## Drag and Drop

You can also support drag/drop using the Drag & Drop API.

If you listen to the `drop` event you'll find `event.dataTransfer.files` (or
`event.originalEvent.dataTransfer.files` given we're using jQuery events).

However you cannot serialize that event as flight data. And unlike how you can
read `e.target.files` there is no way to get at the files like this. The
`e.target` does not have any such file data, the files are attached to the
event, not the node.

That means our upload data component must listen for the drop event on the
document. This makes sense anyway as it allows us to use delegated events. It
does mean that we have to read from the DOM in our data component which kind of
leaks across the ui/data divide, but we'll have to live with that.

So the data component needs selectors to pick the things it should be listening
for drop events on.  A simple class name will do for that.

To get the URL, method and from name we'll look for `data-*` attributes on the
target. That means these have to be predictable.

A possible alternative to this is that the data component listens for a
`uiUploadData` event. So anything listening for `dataUploadQueued` could
immediately trigger a `uiUploadData` with the id. But this seems like a
pointless round trip that could cause more problems than it solves.

Note that because we are doing this on the document we also have to make sure
events are cancelled for `dragenter`, `dragover` and `dragleave`. But you can
still have a UI component attached to the dropzone that listens for these and
does things like highlighting or other UI feedback.

---

There is also `event.dataTransfer.items` which dropzone.js does use. Each item
has methods like `getAsFile`, `getAsString` and `webkitGetAsEntry`. I'm not
sure of the advantage of this over the `files`, which is a `FileList` same as
is attached to the `<input type="file" />`.


---

Seeing as we're doing this for `drop` we may as well do the same thing for
`change` on file inputs. This will get the URL and method from the closest form
and the name from the input itself via `e.target`. That keeps consistency.

We're not yet supporting additional data. I don't want to add any generic form
serializing code to our upload component though. But there really is no need -
because it is a mixin you could add your own `before` or `around` that
serializes the data.  This is where you'd also use validation (although look at
the `dropzone` attribute for this as well, as that can tell you accepted
mime-types and the effect).

---

I had an issue with my dropzone that because `dragenter` and `dragleave` are
triggered one nested elements it might not behave as you expect. Consider this
markup:

        <div class="filedrop">
            <p>Drop files here</p>
        </div>

These events can be triggered on the child element too. So you drag it over
`.filedrop` and `dragenter` fires for that. Then you drag it over the `p` and
`dragenter` fires for that and bubbles to `.filedrop`. Then you drag off the
text, but not out of the box and `dragleave` fires and bubbles to the parent
and removes the highlight!

So there are options here. You could check in your handler that the element firing the
event is the one you care about. In my demo this would be like:

    if (e.target != this.node) {
        return false;
    }

But this can also be done with CSS via `pointer-events` e.g.

    filedrop p {
        pointer-events: none;
    }

## Testing

Writing unit tests is difficult as there is no way to manipulate the selection of a file
within the browser (for good reason!) so this needs to be mocked. There is `jasmine-ajax`
but it doesn't support uploads. Need to write this from scratch!

[I read somewhere]:https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest#Submitting_forms_and_uploading_files
