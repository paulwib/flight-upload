'use strict';

describeMixin('lib/upload', function () {

    // Initialize the component and attach it to the DOM
    beforeEach(function () {
        setupComponent();
    });

    it('should be defined', function () {
        expect(this.component).toBeDefined();
    });

    describe('listening for uiUploadAdded', function () {
        var eventSpy;

        it('triggers dataUploadQueued', function () {
            eventSpy = spyOnEvent(document, 'dataUploadQueued');
            $(this.component.$node).trigger('uiUploadAdded', {
                files: [],
                url: 'foo',
                name: 'foo'
            });
            expect(eventSpy).toHaveBeenTriggeredOn(document);
        });

        it('assigns a guid to the upload', function() {
            expect(eventSpy.mostRecentCall.data.id).toBeDefined();
            expect(eventSpy.mostRecentCall.data.id.length).toBe(36);
        });
    });

});
