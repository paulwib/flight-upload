/**
 * File upload data mixin
 *
 * This doesn't contain any UI, it just listens for a `uiUploadAdded` event
 * and then processes the files.
 */
define(function (require) {

    'use strict';

    /**
     * Local vars
     */
    var queue = [],
        uploading = [],
        filelists = {},
        uploadEventsMap = {
            loadstart: 'dataUploadStart',
            loadend: 'dataUploadEnd',
            load: 'dataUploadSuccess',
            abort: 'dataUploadAbort',
            error: 'dataUploadError',
            timeout: 'dataUploadTimeout',
            progress: 'dataUploadProgress',
        };

    /**
     * Mixin exports
     */
    return upload;

    /**
     * Mixin function
     */
    function upload() {
        this.defaultAttrs({
            parallelUploads: 5,
            fileDropSelector: '.filedrop',
            fileInputSelector: 'input[type="file"]'
        });

        this.after('initialize', function () {
            this.on('uiUploadAdded', onUploadAdded);
            this.on('change', {
                fileInputSelector: onFileInputChange
            });
            this.on('drop', {
                fileDropSelector: onFileDrop
            });
            this.on('dragenter dragleave dragover', {
                fileDropSelector: cancelEvent
            });
            this.addToQueue = addToQueue;
            this.processQueue = processQueue;
        });
    }

    function cancelEvent(e) {
        e.stopPropagation();
        e.preventDefault();
    }

    /**
     * No-op which can be overridden by the component that mixes this in
     *
     * @param object Event
     */
    function serializeData(e) {
        return false;
    }


    /**
     * This handler is really only for testing, in reality you can't serialize
     * a FileList to pass in event data, except in Chrome
     */
    function onUploadAdded(e, upload) {
        this.addToQueue(upload, upload.files || e.target.files);
    }

    /**
     * Triggered by native `change` event
     */
    function onFileInputChange(e) {
        var $el = $(e.target),
            $form = $el.closest('form'),
            upload = {
                url: $form.attr('action'),
                method: $form.attr('method') || 'POST',
                name: $el.attr('name'),
                data: this.serializeData(e)
            };

        this.addToQueue(upload, e.target.files);
    }

    /**
     * Triggered by native `drop` event
     */
    function onFileDrop(e) {
        cancelEvent(e);
        var $el = $(e.target),
            upload = {
                url: $el.data('url'),
                method: $el.data('method') || 'POST',
                name: $el.data('name') || 'files',
                data: this.serializeData(e)
            };

        this.addToQueue(upload, e.originalEvent.dataTransfer.files);
    }

    /**
     * Queue an upload
     *
     * The FileList is taken from `e.target.files`, so uiUploadAdded should be triggered on a file
     * input. The FileList cannot be passed in data as not serializable in all browsers
     * (works in Chrome, but not Firefox).
     *
     * @param string upload.url The URL to submit to
     * @param string upload.name The name in the form data (use an array name[] for multiple)
     * @param string upload.method HTTP method, defaults to POST
     */
    function addToQueue(upload, files) {
        var id = guid();

        filelists[id] = files;
        upload.id = id;
        upload.method = upload.hasOwnProperty('method') ? upload.method : 'POST';
        upload.files = [];

        // Put file data into serializable object
        for(var i=0, l=files.length, file; i < l; i++) {
            file = files[i];
            upload.files.push({
                name: file.name,
                size: file.size,
                type: file.type
            });
        }

        queue.push(upload);
        this.trigger('dataUploadQueued', upload);
        this.processQueue();
    }

	/**
	 * Generate a guid
	 */
	function guid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0,
                v = (c === 'x' ? r : (r&0x3|0x8));
            return v.toString(16);
        });
	}

    /**
     * Return a function to monitor progress of the given upload
     *
     * @param object upload
     */
    function getOnProgress(upload) {
        return function (progressEvent) {
            var eventType = progressEvent.type;

            if (progressEvent.lengthComputable) {
                var loaded = progressEvent.loaded || progressEvent.position; // position is deprecated
                upload.progressPercent = Math.round(100*loaded/progressEvent.total);
                upload.bytesTotal = progressEvent.total;
                upload.bytesSent = loaded;
            }

            if (uploadEventsMap.hasOwnProperty(eventType)) {
                this.trigger(uploadEventsMap[eventType], upload);
            }
        };
    }

    /**
     * Process the next upload in the queue
     */
    function processQueue() {
        if (queue.length === 0) {
            this.trigger('dataUploadQueueComplete');
            return;
        }
        if (uploading.length > this.attr.parallelUploads) {
            this.trigger('dataUploadDelayed', queue[0]);
            return;
        }
        var upload = queue.pop(),
            data = new FormData(),
            xhr = $.ajaxSettings.xhr(),
            files = filelists[upload.id],
            request;

        // Add files to form data - FileList doesn't support forEach
        for(var i=0, l=files.length, file; i < l; i++) {
            file = files[i];
            data.append(upload.name, file, file.name);
        }

        // Add other data to form data - must be an object of key value pairs
        if (typeof upload.data === 'object') {
            Object.keys(upload.data).forEach(function(key) {
                data.append(key, upload.data[key]);
            });
        }

        // Bind upload events for this file
        Object.keys(uploadEventsMap).forEach(function(uploadEvent) {
            xhr.upload.addEventListener(uploadEvent, getOnProgress(upload).bind(this));
        }.bind(this));

        request = $.ajax({
            type: upload.method,
            url: upload.url,
            data: data,
            xhr: function() { return xhr; }, // use the modified xhr
            processData: false,
            contentType: false,
        });

        // Push into the internal progress queue
        uploading.push(upload);

        $.when(request)
            // Triggered for 404 etc.
            .fail(function() {
                this.trigger('dataUploadFailed', upload);
            }.bind(this))
            // Remove file from uploading
            .always(function() {
                uploading = uploading.filter(function(upload) {
                    return upload.id !== upload.id;
                });
                this.processQueue();
            }.bind(this));
    }

});
