/**
 * Demo file list
 */
define(function (require) {

    'use strict';

    /**
     * Module dependencies
     */
    var defineComponent = require('flight/lib/component');

    /**
     * Module exports
     */
    return defineComponent(demoUploadProgress);

    /**
     * Module function
     */
    function demoUploadProgress() {
        this.defaultAttrs({
            fileListSelector: 'ul',
            emptySelector: '.empty'
        });
        this.after('initialize', function() {
            this.showProgressBar = showProgressBar;
            this.hideProgressBar = hideProgressBar;
            this.on(document, 'dataUploadQueued', uploadQueued);
            this.on(document, 'dataUploadComplete', uploadComplete);
            this.on(document, 'dataUploadSuccess', uploadSuccess);
            this.on(document, 'dataUploadProgress', uploadProgress);
            this.on(document, 'dataUploadError', uploadError);
            this.on(document, 'dataUploadFailed', uploadFailed);
            this.on(document, 'dataUploadQueueComplete', uploadQueueComplete);
        });
    }

    function uploadQueued(e, upload) {
        this.$node.find(this.attr.emptySelector).hide();
        this.showProgressBar();
        for(var i=0, l=upload.files.length, file; i < l; i++) {
            file = upload.files[i];
            this.$node.find(this.attr.fileListSelector).append('<li id="file-' +
                upload.guid + '-' + i + '">' + file.name + '</li>');
        }
    }

    function showProgressBar() {
        var $progress = this.$node.find('progress');
        if (!$progress.length) {
            $('<progress />').appendTo(this.$node);
        }
        $progress.show();
    }

    function hideProgressBar() {
        var $progress = this.$node.find('progress');
        $progress.hide();
    }

    function uploadProgress(e, upload) {
        this.$node.find('progress').attr('value', upload.progressPercent/100);
    }

    // Fired when uploaded successfully (from the XMlHttpRequest)
    function uploadSuccess(e, upload) {
        this.$node.find(this.attr.fileListSelector + ' li[id^="file-' + upload.guid + '"]').remove();
    }

    // Fires when the XmlHttpRequestUpload generates an error
    function uploadError(e, upload) {
    }

    // Fired when the XmlHttpRequest fails (like a 404)
    function uploadFailed(e, upload) {
    }

    // Fired whether completed successfully or not
    function uploadComplete(e, upload) {
    }

    // Fired when all uploads finished
    function uploadQueueComplete(e) {
        this.$node.find(this.attr.emptySelector).show();
        this.hideProgressBar();
    }
});

