'use strict';

requirejs.config({
	baseUrl: '',
	paths: {
		'flight': '../bower_components/flight',
		'lib': '../lib'
	},
	urlArgs: 'bust=' + (new Date()).getTime()
});

require(
	[
		'flight/lib/compose',
		'flight/lib/registry',
		'flight/lib/advice',
		'flight/lib/logger',
		'flight/lib/debug'
	],

	function(compose, registry, advice, withLogging, debug) {
		debug.enable(true);
		compose.mixin(registry, [advice.withAdvice, withLogging]);

		require(['js/demo'], function(initialize) {
			initialize();
		});
	}
);

