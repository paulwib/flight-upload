/**
 * This is a demo that mixes in the upload API
 *
 * It doesn't do anythign else but could include things like validation,
 * serialzing additional form data etc.
 */
define(function (require) {

	'use strict';

	/**
	 * Module dependencies
	 */
	var defineComponent = require('flight/lib/component'),
		withUpload = require('lib/upload');

	/**
	 * Module exports
	 */
	return defineComponent(demoUpload, withUpload);

	/**
	 * Module function
	 */
	function demoUpload() {
        this.after('initialize', function(e) {
            this.serializeData = serializeData;
        });
    }

    /**
     * Custom function for serializing data overrides mixin
     */
    function serializeData(e) {
        return {
            x: 'y'
        };
    }
});


