/**
 * Demo file drop
 *
 * This is the UI component that adds and removes a class when you drag files
 * over the dropzone. This could be extended in lots of ways, for example to
 * check that the files much an accepted mime-type (maybe via the dropzone
 * attribute) and cancels the drop event if they don't match. Or displays an
 * error etc.
 */
define(function (require) {

    'use strict';

    /**
     * Module dependencies
     */
    var defineComponent = require('flight/lib/component');

    /**
     * Module exports
     */
    return defineComponent(demoFileDrop);

    /**
     * Module function
     */
    function demoFileDrop() {
        this.after('initialize', function() {
            this.on('dragenter', onDragEnter);
            this.on('dragleave', onDragLeave);
        });
    }

    function onDragEnter(e) {
        this.$node.addClass('highlight');
    }

    function onDragLeave(e) {
        this.$node.removeClass('highlight');
    }
});

