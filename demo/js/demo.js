define(function (require) {

	'use strict';

	/**
	 * Module dependencies
	 */

	var demoUpload = require('js/upload'),
        demoUploadProgress = require('js/upload-progress-ui'),
        demoFileDrop = require('js/filedrop-ui');

	/**
	 * Module exports
	 */

	return initialize;

	/**
	 * Module function
	 */

	function initialize() {
        demoUpload.attachTo(document);
        demoUploadProgress.attachTo('#upload-progress');
        demoFileDrop.attachTo('.filedrop');
	}

});

