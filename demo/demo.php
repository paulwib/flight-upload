<?php

$method = $_SERVER['REQUEST_METHOD'];
if ($method != 'POST') {
    echo 'Please POST a file';
    exit();
}

header('Content-type: text/json', true);
echo json_encode(array($_POST, $_FILES));
